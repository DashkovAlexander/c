﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public class Dice : Game
    {
        private int myPlayerSum;
        private int myCasinoSum;
        public Dice(Croupier croupier, Player player, string logger)
            : base(croupier, player, logger)
        {
            GameType = GameType.Dice;
            RewardFactor = 2;
        }

        protected override void RecordGame()
        {
            GameInfo = myPlayerSum + " " + myCasinoSum;
        }
        public override void Play()
        {
            var random = new Random();
            myPlayerSum = 0;
            Console.WriteLine("Insert your sum(number in range from 2 to 12): ");
            while (myPlayerSum < 1 || myPlayerSum > 12)
            {
                if ((myPlayerSum < 1 || myPlayerSum > 12) && (myPlayerSum != 0))
                {
                    Console.WriteLine("There is no such sum. Enter different number.");
                }
                myPlayerSum = Translator.ReadInt();
            }
            var firstDice = random.Next(1, 7);
            var secondDice = random.Next(1, 7);
            myCasinoSum = firstDice + secondDice;
            Console.WriteLine(firstDice + " " + secondDice);
            Console.WriteLine("The sum is " + myCasinoSum);
            if (myCasinoSum == myPlayerSum)
            {
                IsWinner = true;
            }
        }
    }
}
