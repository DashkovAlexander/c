﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public static class DepositAgent
    {
        public static void WriteDeposit(Player user, StreamWriter writer)
        {
            DateTimeOffset currentTime = DateTimeOffset.Now;
            writer.WriteLine(currentTime + " " + "DepositLog: " + user.Name + " DEPOSITE: " + user.Balance);
            writer.Flush();
        }
        public static Player FindDeposit(string path, string name, bool wereLogged)
        {
            if(!wereLogged) return new Player(name,0);
            FileStream fs;
            int budget = 0;
            using (fs = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                    using (StreamReader sr = new StreamReader(fs))
                    {
                        string playerObject;
                        while (Translator.ReadString(sr, out playerObject))
                        {
                            if(playerObject.Equals("")) continue;
                            string[] objectPlayer = playerObject.Split(' ');
                            if (objectPlayer[3].Equals("DepositLog:") && objectPlayer[4].Equals(name))
                            {
                                try
                                {
                                    budget = int.Parse(objectPlayer[6]);
                                }
                                catch (FormatException)
                                {
                                    Console.WriteLine("Sorry error, in file Bank. Connect with our application developer team.");
                                }
                            }
                        }
                    }
            }
            return new Player(name,budget);
        }

    }
}
