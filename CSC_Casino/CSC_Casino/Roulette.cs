﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public class Roulette : Game
    {
        private int myUserChoice;
        private int myRouletCell;
        private char myColor;
        public Roulette(Croupier croupier, Player player, string logger)
            : base(croupier, player, logger)
        {
            GameType = GameType.Roulet;
        }

        protected override void RecordGame()
        {
            if (RewardFactor == 2)
            {
                GameInfo = myColor + " " + myRouletCell;
                return;
            }
            GameInfo = myUserChoice + " " + myRouletCell;
        }
        public override void Play()
        {
            var random = new Random();
            Console.WriteLine("Would your bet on color or number(c/n):");
            char type;
            while (!Translator.ReadChar('c', 'n', out type)) { }
            if (type == 'c')
            {
                RewardFactor = 2;
                Console.WriteLine("Choose your color(b/r):");
                while (!Translator.ReadChar('b', 'r', out myColor)) { }
                myUserChoice = (myColor == 'b') ? 1 : 2;
                myRouletCell = random.Next(1, 37);
                if (myRouletCell % 2 == myUserChoice % 2)
                {
                    IsWinner = true;
                }
            }
            else
            {
                RewardFactor = 4;
                Console.WriteLine("Please Insert your number:");
                myUserChoice = -1;
                while (!((myUserChoice < 37) && (myUserChoice > 0)))
                {
                    myUserChoice = Translator.ReadInt();
                }
                myRouletCell = random.Next(1, 37);
                if (myRouletCell == myUserChoice)
                {
                    IsWinner = true;
                }
            }
        }

    }
}
