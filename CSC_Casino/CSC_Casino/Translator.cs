﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public static class Translator
    {
        static public bool Confirmation()
        {
            string s = Console.ReadLine();
            if (s.Equals("y") || s.Equals("Y"))
            {
                return true;
            }
            if (s.Equals("n") || s.Equals("N"))
            {
                return false;
            }
            Console.WriteLine("Please insert y/n:");
            return Confirmation();
        }
        static public int ReadInt()
        {
            bool terminate = false;
            int n = 0;
            while (!terminate)
            {
                try
                {
                    n = int.Parse(Console.ReadLine());
                    if (n < 0)
                    {
                        throw new InvalidValueException();
                    }
                    terminate = true;
                }
                catch (Exception ex)
                {
                    if (ex is FormatException || ex is InvalidValueException)
                    {
                        Console.WriteLine("Please insert number again:");
                    }
                }
            }
            return n;
        }
        static public bool ReadChar(char a, char b, out char c)
        {
            c = '0';
            try
            {
                c = char.Parse(Console.ReadLine());
                if (c != a && c != b)
                {
                    Console.WriteLine("Please press " + a + " or " + b + " button");
                    return false;
                }
                return true;
            }
            catch (FormatException)
            {
                Console.WriteLine("Please press " + a + " or " + b + " button:");
                return false;
            }

        }
        static public bool ReadString(StreamReader sr, out string s)
        {
            s = sr.ReadLine();
            if (s != null)
            {
                return true;
            }
            return false;
        }
    }
}
