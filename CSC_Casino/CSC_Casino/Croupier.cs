﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;


namespace CSC_Casino
{
        public class Croupier
        {
            public bool KeepInterested { get; set; }
            private readonly string myLogger;
            private readonly Player myPlayer;
            public Croupier(string log, Player shuller)
            {
                myLogger = log;
                myPlayer = shuller;
                KeepInterested = true;
            }
            public void Suggestion()
            {
                Console.WriteLine("Which game you want to play?");
                Console.WriteLine("1.BlackJack");
                Console.WriteLine("2.Roulette");
                Console.WriteLine("3.Dice");
                Console.WriteLine("4.Exit");
                Console.WriteLine("Write number of chosen game:");
                int choice = Translator.ReadInt();
                Game currentGame = null;  
                    switch (choice)
                    {
                        case 1:
                            currentGame = new BlackJack(this,myPlayer,myLogger);
                            break;
                        case 2:
                            currentGame = new Roulette(this,myPlayer,myLogger);
                            break;
                        case 3:
                            currentGame = new Dice(this,myPlayer,myLogger);
                            break;
                        case 4:
                            KeepInterested = false;
                            break;
                    }
                if (KeepInterested)
                {
                  currentGame.PlayProcess();   
                }
            }
            public void Congratulations()
            {
                Console.WriteLine("Congratulations! You won!");
            }
            public void Sorry()
            {
                Console.WriteLine("Sorry. You lose.");
            }
            public void GoodBye()
            {
                Console.WriteLine("It was a pleasure to play with you.");
                Console.WriteLine("Farewell");
            }
        }
}
