﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Serialization;





namespace CSC_Casino
{
    class Program
    {
        static void Main(string[] args)
        {
            
            string path = @"log.txt";
            bool loggedBefore = true;
            try
            {
                if (!File.Exists(path))
                {
                    loggedBefore = false;
                    File.CreateText(path).Close();
                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Sorry, in order to save your game results, so we need to have acess to C drive of your computer.");
                Console.WriteLine("Change authotrization configuration, and open game again.");
                return;
            }
            Console.WriteLine("Welcome to our Casino!");
            Console.WriteLine("Please enter your name:");
            string name = Console.ReadLine();
            Player player = DepositAgent.FindDeposit(path,name, loggedBefore); 
            Console.WriteLine("Hello, " + name + "!");
            Console.WriteLine(name + ", your budget now is " + player.Balance + "$. Do you want to add some money?(y/n)");
            if (Translator.Confirmation())
            {
                Console.WriteLine("How much money you want to add?");
                player.Balance += Translator.ReadInt();
                using (FileStream fs = new FileStream(path, FileMode.Append))
                {
                    using (StreamWriter sw = new StreamWriter(fs))
                    {
                        DepositAgent.WriteDeposit(player,sw);
                    }
                }
            }
            Croupier croupier = new Croupier(path, player);
            while (croupier.KeepInterested)
            {
               croupier.Suggestion();
            }
            croupier.GoodBye();
            Thread.Sleep(1000);
        }
    }
}
