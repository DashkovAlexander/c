﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public abstract class Game
    {
        protected readonly StreamWriter Writer;
        protected readonly Croupier Croupier;
        protected string GameInfo;
        protected int RewardFactor;
        protected bool IsWinner;
        public string Status;

        protected int Bet { get; set; }
        protected int BalanceChange { get; set; }
        protected GameType GameType { get; set; }
        protected Player Gamer { get; set; }
        

        protected Game(Croupier croupier, Player player, string logger)
        {
            Croupier = croupier;
            Gamer = player;
            IsWinner = false;
            Writer = new StreamWriter(logger, true);
            GameInfo = "";
        }
        private void Betting()
        {
            Console.WriteLine("Please, enter your bet:");
            Bet = Translator.ReadInt();
            if (Bet > Gamer.Balance)
            {
                Writer.Close();
                Writer.Dispose();
                throw new NotEnoughMoneyException();
            }
            Gamer.Balance -= Bet;
        }
        private void Logging(string s)
        {
            DateTimeOffset currentTime = DateTimeOffset.Now;
            Writer.WriteLine(currentTime+" "+s);
        }
        private void DepositLog()
        {
            DepositAgent.WriteDeposit(Gamer,Writer);
        }
        private void BetLog()
        {
            string logString = "BetLog: "+Gamer.Name+" "+GameType+" BET: "+ Bet;
            Logging(logString);
        }
        private void GameLog()
        {
            string logString = "GameLog: "+Gamer.Name+" "+GameType+" "+Status+" "+BalanceChange+" "+GameInfo;
            Logging(logString);
        }
        public abstract void Play();
        protected virtual void GiveReward()
        {
            if (IsWinner)
            {
                Gamer.Balance += RewardFactor*Bet;
                BalanceChange = (RewardFactor - 1)*Bet;
                return;
            }
            BalanceChange = Bet;
        }
        protected abstract void RecordGame();
        public void PlayProcess()
        {
            bool terminate = false;
            while (!terminate)
            {
                try
                {
                    Betting();
                }
                catch (NotEnoughMoneyException)
                {
                    Console.WriteLine("Sorry, you don't have enough money to bet so much!");
                    return;
                }
                BetLog();
                Play();
                if (IsWinner)
                {
                    Croupier.Congratulations();
                    Status = "Win";
                }
                else
                {
                    Croupier.Sorry();
                    Status = "Lose";
                }
                GiveReward();
                RecordGame();
                GameLog();
                DepositLog();
                Console.WriteLine("Play Again?(y/n)");
                if (!Translator.Confirmation()) terminate = true;
            }
            Writer.Close();
            Writer.Dispose();
        }
    }

    public enum GameType
    {
        BJ,Dice,Roulet
    }

    class NotEnoughMoneyException : Exception { }
    class InvalidValueException : Exception { }
}
