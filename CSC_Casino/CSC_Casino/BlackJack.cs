﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public class BlackJack : Game
    {
        private int myPlayerSum;
        private int myDealerSum;
        public BlackJack(Croupier croupier, Player player, string logger)
            : base(croupier, player, logger)
        {
            GameType = GameType.BJ;
            RewardFactor = 2;
        }
        protected override void RecordGame()
        {
            GameInfo = myPlayerSum + " " + myDealerSum;
        }
        public override void Play()
        {
            Random pack = new Random();
            int currentCard = pack.Next(2, 12);
            Console.WriteLine("Price of your first card is " + currentCard);
            myPlayerSum = currentCard;
            myDealerSum = 0;
            currentCard = pack.Next(2, 12);
            Console.WriteLine("Price of your second card is " + currentCard);
            myPlayerSum += currentCard;
            if (myPlayerSum == 21)
            {
                IsWinner = true;
                return;
            }
            Console.WriteLine("Your sum now is " + myPlayerSum);
            Console.WriteLine("More (y/n)?");
            while (Translator.Confirmation())
            {
                myPlayerSum += currentCard;
                Console.WriteLine("Your sum now is " + myPlayerSum);
                if (myPlayerSum == 21)
                {
                    IsWinner = true;
                    return;
                }
                if (myPlayerSum > 21)
                {
                    return;
                }
                else
                {
                    currentCard = pack.Next(2, 12);
                }
                Console.WriteLine("More(y/n)?");
            }
            currentCard = pack.Next(2, 12);
            myDealerSum = currentCard;
            currentCard = pack.Next(2, 12);
            myDealerSum += currentCard;
            Console.WriteLine("Dealer sum is " + myDealerSum);
            if (myDealerSum == myPlayerSum) return;
            if (Math.Abs(myDealerSum - 21) > Math.Abs(myPlayerSum - 21))
            {
                IsWinner = true;
            }
        }
    }
}
