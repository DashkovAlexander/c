﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CSC_Casino
{
    public class Player
    {
        public readonly string Name;
        public int Balance { get; set; }
        public Player(string name, int budget)
        {
            Name = name;
            Balance = budget;
        }
    }
}
